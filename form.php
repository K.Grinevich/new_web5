<!DOCTYPE html>

<style>
/* Сообщения об ошибках и поля с ошибками выводим с красным бордюром. */
.error {
  border: 2px solid red;
}
    </style>
  </head>
  <body>

<?php
if (!empty($messages)) {
  print('<div id="messages">');
  // Выводим все сообщения.
  foreach ($messages as $message) {
    print($message);
  }
  print('</div>');
}

// Далее выводим форму отмечая элементы с ошибками классом error
// и задавая начальные значения элементов ранее сохраненными.
?>

<head>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
  <meta charset="utf-8">
  <title>Задание 5</title>
  <link rel="stylesheet" href="style.css">
</head>

<body>
  <header>
    <div id="logo" class="m-auto">
      <img class="log"
        src="https://fsa.zobj.net/crop.php?r=7GlH5Mv1B5ye6dA3lM5obFLt7yVTs3s-42e9MSG1MpkUnNES7ximzIqJp9T60cG-4BtqKMRHAYYpihKjaAtZjLIQaeLE2hWLrSMPHc35HkLBp6VsrPS9b3REpqdhfLfwHPodVclMEgj26wRC"
        alt="logo" width="100" height="80">
    </div>
    Форма
  </header>
  <div id="main-aside-wrapper">
    <div id="cont" class="container">
      <div id="form" class="col-12 order-lg-3 order-sm-2">
        <div id="vhod">
          <?php 
          if (empty($_SESSION['login'])){
          ?>
          <a href="login.php" >Войти</a>
          
          <?php 
          }else { ?><a href="login.php" >Выйти</a><?php } ?>
          
        </div>
        <form action="" method="POST">
          ФИО:
          <label>
            <input name="fio"  <?php if ($errors['fio']) {print 'class="error"';} ?> value="<?php print $values['fio']; ?>" placeholder="Введите ФИО">
          </label>
          <br>
          <br>
          E-mail:
          <label>
            <input type="email" name="email" placeholder="Введите e-mail" <?php if ($errors['email']) {print 'class="error"';} ?> value="<?php print $values['email']; ?>">
          </label>
          <p>Ваш год рождения:</p>
          <label>
            <select name="yob">
              <option value="1990" <?php if($values['yob'] == 1990) {print 'selected';}?>>1990</option>
              <option value="1991" <?php if($values['yob'] == 1991) {print 'selected';}?>>1991</option>
              <option value="1992" <?php if($values['yob'] == 1992) {print 'selected';}?>>1992</option>
              <option value="1993" <?php if($values['yob'] == 1993) {print 'selected';}?>>1993</option>
              <option value="1994" <?php if($values['yob'] == 1994) {print 'selected';}?>>1994</option>
              <option value="1995" <?php if($values['yob'] == 1995) {print 'selected';}?>>1995</option>
              <option value="1996" <?php if($values['yob'] == 1996) {print 'selected';}?>>1996</option>
              <option value="1997" <?php if($values['yob'] == 1997) {print 'selected';}?>>1997</option>
            </select>
          </label>

          <p>Пол:</p>
          <label>
            <input type="radio" name="radio-pol" value="M" <?php if($values['radio-pol'] == 'M') {print 'checked';}?> <?php if ($errors['radio-pol']) {print 'class="error"';} ?> />М
          </label>
          <label>
            <input type="radio" name="radio-pol" value="W" <?php if($values['radio-pol'] == 'W') {print 'checked';}?> <?php if ($errors['radio-pol']) {print 'class="error"';} ?> />Ж
          </label>

          <p>Количество конечностей</p><br />
          <label>
            <input type="radio" name="radio-kon" value="0" <?php if($values['radio-kon'] == 0) {print 'checked';}?> <?php if ($errors['radio-kon']) {print 'class="error"';} ?> />0
          </label>
          <label>
            <input type="radio" name="radio-kon" value="1" <?php if($values['radio-kon'] == 1) {print 'checked';}?> <?php if ($errors['radio-kon']) {print 'class="error"';} ?>/>1
          </label>
          <label>
            <input type="radio" name="radio-kon" value="2" <?php if($values['radio-kon'] == 2) {print 'checked';}?> <?php if ($errors['radio-kon']) {print 'class="error"';} ?>/>2
          </label>
          <label>
            <input type="radio" name="radio-kon" value="3" <?php if($values['radio-kon'] == 3) {print 'checked';}?> <?php if ($errors['radio-kon']) {print 'class="error"';} ?>/>3
          </label>
          <label>
            <input type="radio" name="radio-kon" value="4" <?php if($values['radio-kon'] == 4) {print 'checked';}?> <?php if ($errors['radio-kon']) {print 'class="error"';} ?> />4
          </label>
          <label>
            <input type="radio" name="radio-kon" value="5" <?php if($values['radio-kon'] == 5) {print 'checked';}?> <?php if ($errors['radio-kon']) {print 'class="error"';} ?> />5
          </label>

          <p>Сверхспособности</p>
          <label>
            <select name="sp-sp[]" multiple=multiple>
              <option value="1" <?php if ($errors['sp-sp']) {print 'class="error"';} ?>
            <?php
              $arr = str_split($values['sp-sp']);
              foreach($arr as $el)
                if ($el == 1)
                  print 'selected';
            ?>
              >Бессмертие</option>
              <option value="2" <?php if ($errors['sp-sp']) {print 'class="error"';} ?>
              <?php
              $arr = str_split($values['sp-sp']);
              foreach($arr as $el)
                if ($el == 2)
                  print 'selected';
            ?>
              > Прохождение сквозь стены</option>
              <option value="3" <?php if ($errors['sp-sp']) {print 'class="error"';} ?>
              <?php
              $arr = str_split($values['sp-sp']);
              foreach($arr as $el)
                if ($el == 3)
                  print 'selected';
            ?>
              >Левитация</option>
            </select>
          </label>

          <p id="bio">Биография</p>
          <label>
            <textarea placeholder="Расскажите о себе" name="biography" rows="6" cols="60"  <?php if ($errors['biography']) {print 'class="error"';} ?> ><?php print $values['biography'];?></textarea>
          </label>
          <br>

          <label>
            С контрактом ознакомлен
            <input type="checkbox" name="ok" checked/>
          </label>
          <br>
          <input type="submit" value="Отправить">
        </form>
      </div>
    </div>
  </div>
</body>

</html>